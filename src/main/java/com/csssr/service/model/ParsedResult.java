package com.csssr.service.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * This class contains the result of the {@link ParseRequest} object parsing.
 *
 * @author Eugene Makarenko
 * @see ParseRequest
 * @see Group
 */
@ApiModel(value = "Response", description = "The sorted and filtered parsed string")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParsedResult {
  @ApiModelProperty(value = "The settings that was used to parse the string")
  private ParseSettings settings;

  @ApiModelProperty(value = "The groups with elements")
  private List<Group> groups;
}
