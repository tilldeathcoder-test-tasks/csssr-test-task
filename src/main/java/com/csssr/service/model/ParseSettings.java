package com.csssr.service.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * This class contains settings for the string parsing.
 *
 * @author Eugene Makarenko
 */
@ApiModel(value = "Settings", description = "The settings for the string parser")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ParseSettings {

  @ApiModelProperty(value = "The separator to split the string. Can't be blank", required = true)
  @NotEmpty(message = "{notNullOrEmpty}")
  private String separator;

  @ApiModelProperty(
      value =
          "The minimal count of found elements in the group. The response will return groups that have more elements that was provided by this value. Can't be null or negative",
      required = true,
      allowableValues = "range[0, 9223372036854775807]")
  @NotNull(message = "{notNullOrEmpty}")
  @Positive(message = "{notNegative}")
  private long minElementsCount;
}
