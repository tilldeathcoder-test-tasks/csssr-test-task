package com.csssr.service.impl;

import com.csssr.service.ParseService;
import com.csssr.service.model.Group;
import com.csssr.service.model.ParseRequest;
import com.csssr.service.model.ParsedResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * The implementation of the {@link ParseService} interface.
 *
 * @author Eugene Makarenko
 * @see ParseService
 * @see ParseRequest
 * @see ParsedResult
 */
@Service
@Slf4j
public class ParseServiceImpl implements ParseService {

  private Comparator<String> elementsComparator;

  @Autowired
  public ParseServiceImpl(Comparator<String> elementsComparator) {
    this.elementsComparator = elementsComparator;
  }

  @Override
  public ParsedResult parse(ParseRequest parseRequest) {
    log.info("Request: {}", parseRequest);
    ParsedResult result =
        ParsedResult.builder()
            .settings(parseRequest.getSettings())
            .groups(Collections.emptyList())
            .build();

    String[] parsedString = parseStringBySeparator(parseRequest);
    List<Group> groups =
        sortParsedStringByGroups(parseRequest.getSettings().getMinElementsCount(), parsedString);

    result.setGroups(groups);
    log.info("Result: {}", result);
    return result;
  }

  private String[] parseStringBySeparator(ParseRequest parseRequest) {
    String stringToParse = parseRequest.getStringToParse().toLowerCase();
    return stringToParse.split(Pattern.quote(parseRequest.getSettings().getSeparator()));
  }

  private List<Group> sortParsedStringByGroups(long minElementsCount, String[] parsedString) {
    if (parsedString.length == 1 && parsedString.length > minElementsCount) {
      return Collections.singletonList(buildGroup(parsedString[0]));
    }

    TreeMap<Character, TreeSet<String>> labelToElements = new TreeMap<>();
    Arrays.stream(parsedString)
        .forEach(
            world -> {
              char currentLabel = world.charAt(0);
              TreeSet<String> elements = labelToElements.get(currentLabel);
              if (isEmpty(elements)) {
                elements = new TreeSet<>(elementsComparator);
                labelToElements.put(currentLabel, elements);
              }
              elements.add(world);
            });

    return convertAndFilterMapToList(minElementsCount, labelToElements);
  }

  private Group buildGroup(String world) {
    List<String> elements = new ArrayList<>();
    elements.add(world);
    return Group.builder().label(world.charAt(0)).elements(elements).build();
  }

  private List<Group> convertAndFilterMapToList(
      long minElementsCount, TreeMap<Character, TreeSet<String>> labelToElements) {
    return labelToElements.values().stream()
        .map(this::groupMapper)
        .filter(groupFilter(minElementsCount))
        .collect(Collectors.toList());
  }

  private Group groupMapper(TreeSet<String> value) {
    return Group.builder().label(value.first().charAt(0)).elements(new ArrayList<>(value)).build();
  }

  private Predicate<Group> groupFilter(long minElementsCount) {
    return group -> group.getElements().size() > minElementsCount;
  }
}
