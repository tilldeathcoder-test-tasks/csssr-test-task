package com.csssr.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
@Import({SpringDataRestConfiguration.class})
public class SwaggerSpringConfiguration {

  @Bean
  public Docket api(ApiInfo apiInfo) {
    Docket bean =
        new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.csssr.web.controller"))
            .paths(PathSelectors.any())
            .build();
    bean.useDefaultResponseMessages(false);

    return bean;
  }

  @Bean
  public ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("The string parse service")
        .description("This service parses input string by provided settings")
        .version("1.0.0")
        .contact(
            new Contact(
                "Eugene Makarenko",
                "https://www.gitlab.com/tilldeathcoder",
                "eugene.makarenko.work@gmail.com"))
        .build();
  }
}
