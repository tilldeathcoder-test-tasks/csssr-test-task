package com.csssr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The entry point of the application
 *
 * @author Eugene Makarenko
 * @see SpringApplication
 */
@SpringBootApplication
public class CSSSRTestTaskApplication {

  public static void main(String[] args) {
    SpringApplication.run(CSSSRTestTaskApplication.class, args);
  }
}
