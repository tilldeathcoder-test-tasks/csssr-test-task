package com.csssr.controller;

import com.csssr.CSSSRTestTaskApplication;
import com.csssr.web.controller.v1.ParseController;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * The parent class to test {@link ParseController}. The API version is v1.
 *
 * @author Eugene Makarenko
 * @see ParseController
 */
@SpringBootTest(
    classes = {CSSSRTestTaskApplication.class},
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class ParseControllerBaseIT {

  private static final String PARSE_URL_PART = "api/v1/parse";

  @Value("${server.servlet.context-path}")
  protected String basePath;

  @Value("${server.port}")
  private int port;

  @BeforeEach
  public void setUp() {
    RestAssured.port = port;
  }

  protected String getParsePath() {
    return basePath + PARSE_URL_PART;
  }
}
